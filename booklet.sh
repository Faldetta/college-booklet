#! /bin/zsh

function handler(){
    docker-compose down
}

trap handler SIGINT

docker-compose up -d
java -jar target/booklet-0.0.1-SNAPSHOT.jar

