# Pipeline Results

## [Results Webpage](https://faldetta.gitlab.io/college-booklet/)

### Code Coverage

- [Codecov Code Coverage](https://app.codecov.io/gl/Faldetta/college-booklet)
- [Jacoco Code Coverage](https://faldetta.gitlab.io/college-booklet/jacoco/index.html)

###  Mutation Testing

- [PIT Mutation Testing](https://faldetta.gitlab.io/college-booklet/pit/index.html)

###  Code Quality

- [SonarCloud Code Quality](https://sonarcloud.io/dashboard?id=Faldetta_college-booklet)
- [GitLab-CodeClimate CodeQuality](https://faldetta.gitlab.io/college-booklet/gl-code-quality-report.html)
