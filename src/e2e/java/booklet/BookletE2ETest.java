package booklet;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import io.github.bonigarcia.wdm.WebDriverManager;

class BookletE2ETest {

	private static int port = Integer.parseInt(System.getProperty("server.port", "8080"));
	private static String baseUrl = "http://localhost:" + port;
	static private WebDriver driver;
	static private ChromeOptions options = new ChromeOptions().setHeadless(true);

	@BeforeAll
	static void beforeAll_setup() {
		WebDriverManager.chromedriver().setup();
		driver = new ChromeDriver(options);
		driver.get(baseUrl + "/new-user");
		driver.findElement(By.name("username")).sendKeys("userTest");
		driver.findElement(By.name("password")).sendKeys("password");
		driver.findElement(By.name("name")).sendKeys("Test");
		driver.findElement(By.name("surname")).sendKeys("EtoE");
		driver.findElement(By.name("collegeId")).sendKeys("0");
		driver.findElement(By.name("btn-submit")).click();
		driver.quit();
	}

	@BeforeEach
	void setup() {
		driver = new ChromeDriver(options);
	}

	@AfterEach
	void teardown() {
		driver.quit();
	}

	@Test
	void test_SignupAndLogin() {
		driver.get(baseUrl + "/new-user");
		driver.findElement(By.name("username")).sendKeys("username");
		driver.findElement(By.name("password")).sendKeys("password");
		driver.findElement(By.name("name")).sendKeys("Checco");
		driver.findElement(By.name("surname")).sendKeys("Balestrini");
		driver.findElement(By.name("collegeId")).sendKeys("1");
		driver.findElement(By.name("btn-submit")).click();
		driver.findElement(By.name("username")).sendKeys("username");
		driver.findElement(By.name("password")).sendKeys("password");
		driver.findElement(By.name("btn-login")).click();
		assertThat(driver.findElement(By.id("studentName")).getText()).contains("Checco");
		assertThat(driver.findElement(By.id("studentSurname")).getText()).contains("Balestrini");
		assertThat(driver.findElement(By.id("studentCollegeId")).getText()).contains("1");
	}

	@Test
	void test_AddExam() {
		driver.get(baseUrl + "/login");
		driver.findElement(By.name("username")).sendKeys("userTest");
		driver.findElement(By.name("password")).sendKeys("password");
		driver.findElement(By.name("btn-login")).click();
		driver.findElement(By.id("new")).click();
		driver.findElement(By.name("name")).clear();
		driver.findElement(By.name("name")).sendKeys("analisi");
		driver.findElement(By.name("date")).clear();
		driver.findElement(By.name("date")).sendKeys("12/12/2012");
		driver.findElement(By.name("cfu")).clear();
		driver.findElement(By.name("cfu")).sendKeys("12");
		driver.findElement(By.name("grade")).clear();
		driver.findElement(By.name("grade")).sendKeys("27");
		driver.findElement(By.name("laude")).sendKeys("false");
		driver.findElement(By.name("btn_submit")).click();
		assertThat(driver.findElement(By.id("student.examList_table")).getText()).contains("analisi", "12/12/2012",
				"12", "27", "false");
		driver.findElement(By.id("delete")).click();
	}

	@Test
	void test_DeleteExam() {
		driver.get(baseUrl + "/login");
		driver.findElement(By.name("username")).sendKeys("userTest");
		driver.findElement(By.name("password")).sendKeys("password");
		driver.findElement(By.name("btn-login")).click();
		driver.findElement(By.id("new")).click();
		driver.findElement(By.name("name")).clear();
		driver.findElement(By.name("name")).sendKeys("analisi");
		driver.findElement(By.name("date")).clear();
		driver.findElement(By.name("date")).sendKeys("12/12/2012");
		driver.findElement(By.name("cfu")).clear();
		driver.findElement(By.name("cfu")).sendKeys("12");
		driver.findElement(By.name("grade")).clear();
		driver.findElement(By.name("grade")).sendKeys("27");
		driver.findElement(By.name("laude")).sendKeys("false");
		driver.findElement(By.name("btn_submit")).click();
		driver.findElement(By.id("delete")).click();
		driver.findElement(By.id("noExam"));
	}

	@Test
	void test_Logout() {
		driver.get(baseUrl + "/login");
		driver.findElement(By.name("username")).sendKeys("userTest");
		driver.findElement(By.name("password")).sendKeys("password");
		driver.findElement(By.name("btn-login")).click();
		driver.findElement(By.id("logout")).click();
		driver.getTitle().contains("Login");
	}

	@Test
	void test_NoAccount() {
		driver.get(baseUrl + "/login");
		driver.findElement(By.id("noAccount")).click();
		driver.getTitle().contains("Signup");
	}
}
