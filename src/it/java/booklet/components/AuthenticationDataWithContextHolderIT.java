package booklet.components;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class AuthenticationDataWithContextHolderIT {

	@Autowired
	private AuthenticationDataWithContextHolder authenticationData;

	@BeforeEach
	void setup() {
		SecurityContextHolder.clearContext();
		SecurityContext securityContext = SecurityContextHolder.getContext();
		Authentication auth = new UsernamePasswordAuthenticationToken("username", "password");
		securityContext.setAuthentication(auth);
	}

	@Test
	void test_getAuthenticatedUsername() {
		assertThat(authenticationData.getAuthenticatedUsername()).isEqualTo("username");
	}

}
