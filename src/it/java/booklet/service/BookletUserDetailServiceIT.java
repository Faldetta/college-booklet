package booklet.service;

import static org.assertj.core.api.Assertions.assertThat;

import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.util.ArrayList;
import java.util.Collection;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import booklet.model.User;
import booklet.persistence.UserRepository;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class BookletUserDetailServiceIT {

	@Autowired
	private BookletUserDetailsService userDetailService;

	@Autowired
	private UserRepository userRepository;

	@BeforeEach
	void setup() {
		userRepository.deleteAll();
	}

	@Test
	void test_BookletUserDetailService_throwsUsernameNotFoudExceptionWithMissingUsername() {
		assertThatThrownBy(() -> userDetailService.loadUserByUsername("username"))
				.isInstanceOf(UsernameNotFoundException.class).hasMessage("Username 'username' not found");
	}

	@Test
	void test_BookletUserDetailService_returnsMatchingUserDetails() {
		userRepository.save(new User("username", "password", "studentId"));
		Collection<GrantedAuthority> grantedAuthorities = new ArrayList<>();
		grantedAuthorities.add(new SimpleGrantedAuthority("STUDENT"));

		assertThat(userDetailService.loadUserByUsername("username")).isEqualTo(
				new org.springframework.security.core.userdetails.User("username", "password", grantedAuthorities));
	}

}
