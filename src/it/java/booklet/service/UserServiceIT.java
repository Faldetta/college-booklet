package booklet.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.util.NoSuchElementException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import booklet.model.User;
import booklet.persistence.UserRepository;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class UserServiceIT {

	@Autowired
	private UserService userService;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@BeforeEach
	void setup() {
		userRepository.deleteAll();
	}

	@Test
	void test_findUserByUsername_throwsExceptionWithoutAMatch() {
		assertThatThrownBy(() -> userService.findUserByUsername("username")).isInstanceOf(NoSuchElementException.class);
	}

	@Test
	void test_findUserByUsername_returnsUserIfPresent() {
		userRepository.save(new User("username", passwordEncoder.encode("password"), "studentId"));
		User user = userService.findUserByUsername("username");

		assertThat(user.getUsername()).isEqualTo("username");
		assertThat(user.getStudentId()).isEqualTo("studentId");
		assertThat(passwordEncoder.matches("password", user.getPassword())).isTrue();
	}

	@Test
	void test_addUser_worksWithDatabaseAndPasswordEncoder() {
		userService.addUser("username", "password", "studentId");

		User user = userRepository.findById("username").orElseThrow();

		assertThat(user.getUsername()).isEqualTo("username");
		assertThat(user.getStudentId()).isEqualTo("studentId");
		assertThat(passwordEncoder.matches("password", user.getPassword())).isTrue();
	}
	
	@Test
	void test_existsByUsername_worksWithDatabase() {
		userRepository.save(new User("username", passwordEncoder.encode("password"), "studentId"));
		
		assertThat(userService.existsByUsername("username")).isTrue();
		assertThat(userService.existsByUsername("usersurname")).isFalse();
	}
	
	

}
