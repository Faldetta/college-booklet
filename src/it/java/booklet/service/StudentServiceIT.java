package booklet.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.util.ArrayList;
import java.util.Collection;
import java.util.NoSuchElementException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import booklet.model.Exam;
import booklet.model.Student;
import booklet.persistence.StudentRepository;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class StudentServiceIT {
	
	@Autowired
	private StudentService studentService;
	
	@Autowired
	private StudentRepository studentRepository;

	private final String STUDENT_NAME = "Checco";
	private final String STUDENT_SURNAME = "Balestrini";
	private final String STUDENT_COLLEGE_ID = "01";
	private final String STUDENT_INTERNAL_ID = "CheccoBalestrini01";
	private final String STUDENT_INTERNAL_ID_TO_SAME = "CheccoBalestrini01-1";

	@BeforeEach
	void setup() {
		studentRepository.deleteAll();
	}

	@Test
	void test_getStudentById() {
		Student student = new Student(STUDENT_INTERNAL_ID, STUDENT_NAME, STUDENT_SURNAME, STUDENT_COLLEGE_ID);
		studentRepository.save(student);
		Student studentTest = studentService.getStudentById(STUDENT_INTERNAL_ID);
		
		assertThat(studentTest).isEqualTo(student);
	}

	@Test
	void test_getStudentByIdW_withoutStudent() {
		assertThatThrownBy(() -> studentService.getStudentById(STUDENT_INTERNAL_ID))
				.isInstanceOf(NoSuchElementException.class);
	}

	@Test
	void test_addStudent() {
		studentService.addStudent(STUDENT_NAME, STUDENT_SURNAME, STUDENT_COLLEGE_ID);
		Student studentTest = studentService.getStudentById(STUDENT_INTERNAL_ID);
		
		assertThat(studentTest.getId()).isEqualTo(STUDENT_INTERNAL_ID);
	}

	@Test
	void test_addStudent_withTwoSameNameSurnameCollegeId() {
		studentService.addStudent(STUDENT_NAME, STUDENT_SURNAME, STUDENT_COLLEGE_ID);
		studentService.addStudent(STUDENT_NAME, STUDENT_SURNAME, STUDENT_COLLEGE_ID);
		Student studentTest = studentService.getStudentById(STUDENT_INTERNAL_ID);
		Student studentTestSimilar = studentService.getStudentById(STUDENT_INTERNAL_ID_TO_SAME);
		
		assertThat(studentTest.getId()).isEqualTo(STUDENT_INTERNAL_ID);
		assertThat(studentTestSimilar.getId()).isEqualTo(STUDENT_INTERNAL_ID_TO_SAME);

	}

	@Test
	void test_removeStudentById() {
		studentService.addStudent(STUDENT_NAME, STUDENT_SURNAME, STUDENT_COLLEGE_ID);
		studentService.removeStudentById(STUDENT_INTERNAL_ID);
		
		assertThatThrownBy(() -> studentService.getStudentById(STUDENT_INTERNAL_ID))
		.isInstanceOf(NoSuchElementException.class);
	}
	
	
	@Test
	void test_AddExamToStudentById() {
		Collection<Exam> examList = new ArrayList<>();
		Exam exam = new Exam("Logic 101", "29/02/2020", 9, 26, false);
		examList.add(exam);
		studentService.addStudent(STUDENT_NAME, STUDENT_SURNAME, STUDENT_COLLEGE_ID);
		studentService.addExamToStudentById(STUDENT_INTERNAL_ID, exam);
		Student studentTest = studentService.getStudentById(STUDENT_INTERNAL_ID);
		
		assertThat(examList).isEqualTo(studentTest.getExamList());
	}
	
	
	@Test
	void test_removeExamByNameToStudentById() {
		Exam exam = new Exam("Logic 101", "29/02/2020", 9, 26, false);
		studentService.addStudent(STUDENT_NAME, STUDENT_SURNAME, STUDENT_COLLEGE_ID);
		studentService.addExamToStudentById(STUDENT_INTERNAL_ID, exam);
		Student studentTest = studentService.removeExamByNameToStudentById(STUDENT_INTERNAL_ID, exam.getName());
		Collection<Exam> examList = new ArrayList<>();
		
		assertThat(examList).isEqualTo(studentTest.getExamList());
	}
		
}




