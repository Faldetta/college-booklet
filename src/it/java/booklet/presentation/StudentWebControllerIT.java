package booklet.presentation;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import booklet.model.Exam;
import booklet.model.Student;
import booklet.model.User;
import booklet.persistence.StudentRepository;
import booklet.persistence.UserRepository;

@ExtendWith(SpringExtension.class)
@WithMockUser(username = "username")
@SpringBootTest()
class StudentWebControllerIT {

	@Autowired
	private WebApplicationContext context;

	@Autowired
	private StudentRepository studentRepository;

	@Autowired
	private UserRepository userRepository;

	private MockMvc mvc;

	@BeforeEach
	void setup() {
		mvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();
		userRepository.deleteAll();
		studentRepository.deleteAll();
		userRepository.save(new User("username", "password", "CheccoBalestrini1"));
		studentRepository.save(new Student("CheccoBalestrini1", "Checco", "Balestrini", "1"));
	}

	@Test
	void test_index() throws Exception {
		mvc.perform(get("/")).andExpect(view().name("index"))
				.andExpect(model().attribute("student", new Student("CheccoBalestrini1", "Checco", "Balestrini", "1")));
	}

	@Test
	void test_saveExam() throws Exception {
		mvc.perform(post("/save").param("name", "Logic").param("date", "12/12/2012").param("cfu", String.valueOf(9))
				.param("grade", String.valueOf(26)).param("laude", "false").with(csrf()))
				.andExpect(view().name("redirect:/"));
		assertThat(studentRepository.findById("CheccoBalestrini1").get().getExamList())
				.containsOnly(new Exam("Logic", "12/12/2012", 9, 26, false));
	}

	@Test
	void test_deleteExam() throws Exception {
		studentRepository.deleteAll();
		Student student = new Student("CheccoBalestrini1", "Checco", "Balestrini", "1");
		student.getExamList().add(new Exam("Logic", "12/12/2012", 9, 26, false));
		studentRepository.save(student);

		assertThat(studentRepository.findById("CheccoBalestrini1").get().getExamList()).isNotEmpty();
		mvc.perform(get("/delete?id=Logic")).andExpect(view().name("redirect:/"));
		assertThat(studentRepository.findById("CheccoBalestrini1").get().getExamList()).isEmpty();
	}

}
