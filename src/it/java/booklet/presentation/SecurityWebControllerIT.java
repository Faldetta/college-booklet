package booklet.presentation;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestBuilders.formLogin;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.authenticated;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import booklet.model.Student;
import booklet.model.User;
import booklet.persistence.StudentRepository;
import booklet.persistence.UserRepository;

@ExtendWith(SpringExtension.class)
@SpringBootTest()
class SecurityWebControllerIT {

	@Autowired
	private WebApplicationContext context;

	@Autowired
	private StudentRepository studentRepository;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private PasswordEncoder passwordEncoder;

	private MockMvc mvc;

	@BeforeEach
	void setup() {
		mvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();
		userRepository.deleteAll();
		studentRepository.deleteAll();
	}

	@Test
	void test_saveUser() throws Exception {
		mvc.perform(post("/save-user").param("username", "username").param("password", "password")
				.param("name", "Checco").param("surname", "Balestrini").param("collegeId", "1").with(csrf()))
				.andExpect(view().name("bookletLogin"));
		assertThat(userRepository.existsById("username")).isTrue();
		assertThat(studentRepository.existsById("CheccoBalestrini1")).isTrue();
	}

	@Test
	void test_login() throws Exception {
		userRepository.save(new User("username", passwordEncoder.encode("password"), "CheccoBalestrini1"));
		studentRepository.save(new Student("CheccoBalestrini1", "Checco", "Balestrini", "1"));

		mvc.perform(formLogin().user("username").password("password")).andExpect(authenticated());
	}
}
