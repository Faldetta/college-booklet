package booklet.presentation;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InOrder;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import booklet.model.Student;
import booklet.model.StudentDTO;
import booklet.model.User;
import booklet.model.UserDTO;
import booklet.service.StudentService;
import booklet.service.UserService;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = SecurityWebController.class)
class SecurityWebControllerTest {

	private static final String USER_PASSWORD = "password";
	private static final String USER_USERNAME = "username";

	private static final String STUDENT_COLLEGE_ID = "collegeId";
	private static final String STUDENT_SURNAME = "surname";
	private static final String STUDENT_NAME = "name";
	private static final String STUDENT_INTERNAL_ID = "internalId";

	private static final String MESSAGE_ATTRIBUTE_NAME = "message";
	private static final String STUDENT_ATTRIBUTE_NAME = "student";
	private static final String USER_ATTRIBUTE_NAME = "user";

	private static final String SIGNUP_VIEW = "signup";
	private static final String LOGIN_VIEW = "bookletLogin";

	private static final String LOGIN_MAPPING = "/login";
	private static final String NEW_USER_MAPPING = "/new-user";
	private static final String SAVE_USER_MAPPING = "/save-user";

	private static final MultiValueMap<String, String> postParams = new LinkedMultiValueMap<String, String>() {
		private static final long serialVersionUID = 4731575999831002549L;
		{
			add("username", USER_USERNAME);
			add("password", USER_PASSWORD);
			add("name", STUDENT_NAME);
			add("surname", STUDENT_SURNAME);
			add("collegeId", STUDENT_COLLEGE_ID);
		}
	};

	@MockBean
	private UserService userService;

	@MockBean
	private StudentService StudentService;

	@Autowired
	private MockMvc mvc;

	private void mockAddStudentAndUser() {
		when(StudentService.addStudent(anyString(), anyString(), anyString()))
				.thenReturn(new Student(STUDENT_INTERNAL_ID, STUDENT_NAME, STUDENT_SURNAME, STUDENT_COLLEGE_ID));
		when(userService.addUser(anyString(), anyString(), anyString()))
				.thenReturn(new User(USER_USERNAME, USER_PASSWORD, STUDENT_INTERNAL_ID));
	}

	@Test
	void test_login_status200() throws Exception {
		mvc.perform(get(LOGIN_MAPPING)).andExpect(status().is2xxSuccessful());
	}

	@Test
	void test_login_returnLoginView() throws Exception {
		mvc.perform(get(LOGIN_MAPPING)).andExpect(view().name(LOGIN_VIEW));
	}

	@Test
	void test_newUser_status200() throws Exception {
		mvc.perform(get(NEW_USER_MAPPING)).andExpect(status().is2xxSuccessful());
	}

	@Test
	void test_newUser_returnSignupView() throws Exception {
		mvc.perform(get(NEW_USER_MAPPING)).andExpect(view().name(SIGNUP_VIEW));
	}

	@Test
	void test_newUser_hasUserInModel() throws Exception {
		mvc.perform(get(NEW_USER_MAPPING)).andExpect(model().attribute(USER_ATTRIBUTE_NAME, new UserDTO()));
	}

	@Test
	void test_newUser_hasStudentInModel() throws Exception {
		mvc.perform(get(NEW_USER_MAPPING)).andExpect(model().attribute(STUDENT_ATTRIBUTE_NAME, new StudentDTO()));
	}

	@Test
	void test_newUser_hasMessageInModel() throws Exception {
		mvc.perform(get(NEW_USER_MAPPING)).andExpect(model().attribute(MESSAGE_ATTRIBUTE_NAME, ""));
	}

	@Test
	void test_saveUser_status200WithNoEqualExistingUsername() throws Exception {
		when(userService.existsByUsername(anyString())).thenReturn(false);
		mockAddStudentAndUser();

		mvc.perform(post(SAVE_USER_MAPPING).params(postParams).with(csrf())).andExpect(status().is2xxSuccessful());
	}

	@Test
	void test_saveUser_status200WithEqualExistingUsername() throws Exception {
		when(userService.existsByUsername(anyString())).thenReturn(true);

		mvc.perform(post(SAVE_USER_MAPPING).params(postParams).with(csrf())).andExpect(status().is2xxSuccessful());
	}

	@Test
	void test_saveUser_returnLoginViewWithNoEqualExistingUsername() throws Exception {
		when(userService.existsByUsername(anyString())).thenReturn(false);
		mockAddStudentAndUser();

		mvc.perform(post(SAVE_USER_MAPPING).params(postParams).with(csrf())).andExpect(view().name(LOGIN_VIEW));
	}

	@Test
	void test_saveUser_checkUserNameFromUserService() throws Exception {
		when(userService.existsByUsername(anyString())).thenReturn(true);

		mvc.perform(post(SAVE_USER_MAPPING).params(postParams).with(csrf()));

		verify(userService).existsByUsername(USER_USERNAME);

	}

	@Test
	void test_saveUser_checkNoMoreInteractionsAfterExistingUsernameFound() throws Exception {
		when(userService.existsByUsername(anyString())).thenReturn(true);

		mvc.perform(post(SAVE_USER_MAPPING).params(postParams).with(csrf()));

		verify(userService).existsByUsername(USER_USERNAME);
		verifyNoMoreInteractions(userService);
		verifyNoInteractions(StudentService);
	}

	@Test
	void test_saveUser_reloadSignupPageIfUsernameAlreadyExists() throws Exception {
		when(userService.existsByUsername(anyString())).thenReturn(true);

		mvc.perform(post(SAVE_USER_MAPPING).params(postParams).with(csrf())).andExpect(view().name(SIGNUP_VIEW));
	}

	@Test
	void test_saveUser_reloadSignupPageIfUsernameAlreadyExistsWithErrorMessage() throws Exception {
		when(userService.existsByUsername(anyString())).thenReturn(true);

		mvc.perform(post(SAVE_USER_MAPPING).params(postParams).with(csrf()))
				.andExpect(model().attribute(MESSAGE_ATTRIBUTE_NAME, "Username already used"));
	}

	@Test
	void test_saveUser_reloadSignupPageIfUsernameAlreadyExistsWithUserAndStudentAttributes() throws Exception {
		when(userService.existsByUsername(anyString())).thenReturn(true);

		mvc.perform(post(SAVE_USER_MAPPING).params(postParams).with(csrf()))
				.andExpect(model().attribute(STUDENT_ATTRIBUTE_NAME, new StudentDTO()))
				.andExpect(model().attribute(USER_ATTRIBUTE_NAME, new UserDTO()));
	}

	@Test
	void test_saveUser_addStudenttIfExistingUsernameIsNotFound() throws Exception {
		when(userService.existsByUsername(anyString())).thenReturn(false);
		mockAddStudentAndUser();

		mvc.perform(post(SAVE_USER_MAPPING).params(postParams).with(csrf()));

		verify(StudentService).addStudent(STUDENT_NAME, STUDENT_SURNAME, STUDENT_COLLEGE_ID);
	}

	@Test
	void test_saveUser_addUserIfExistingUsernameIsNotFound() throws Exception {
		when(userService.existsByUsername(anyString())).thenReturn(false);
		mockAddStudentAndUser();

		mvc.perform(post(SAVE_USER_MAPPING).params(postParams).with(csrf()));

		verify(userService).addUser(USER_USERNAME, USER_PASSWORD, STUDENT_INTERNAL_ID);
	}

	@Test
	void test_saveUser_useServiceLayerInTheRightOrderToSaveUserAndStudentIfExistingUsernameIsNotFound()
			throws Exception {
		when(userService.existsByUsername(anyString())).thenReturn(false);
		mockAddStudentAndUser();

		mvc.perform(post(SAVE_USER_MAPPING).params(postParams).with(csrf()));

		InOrder inOrder = Mockito.inOrder(userService, StudentService);
		inOrder.verify(userService).existsByUsername(USER_USERNAME);
		inOrder.verify(StudentService).addStudent(STUDENT_NAME, STUDENT_SURNAME, STUDENT_COLLEGE_ID);
		inOrder.verify(userService).addUser(USER_USERNAME, USER_PASSWORD, STUDENT_INTERNAL_ID);
	}

}
