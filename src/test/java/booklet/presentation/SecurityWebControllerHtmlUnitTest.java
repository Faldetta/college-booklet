package booklet.presentation;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.net.MalformedURLException;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.DomElement;
import com.gargoylesoftware.htmlunit.html.DomNodeList;
import com.gargoylesoftware.htmlunit.html.HtmlForm;
import com.gargoylesoftware.htmlunit.html.HtmlPage;

import booklet.components.AuthenticationData;
import booklet.model.Student;
import booklet.model.User;
import booklet.service.StudentService;
import booklet.service.UserService;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = SecurityWebController.class)
class SecurityWebControllerHtmlUnitTest {

	@MockBean
	private StudentService studentService;

	@MockBean
	private UserService userService;

	@MockBean
	private AuthenticationData authenticationData;

	@Autowired
	private WebClient webClient;

	@Test
	void test_bookletLogin_title() throws FailingHttpStatusCodeException, MalformedURLException, IOException {
		final HtmlPage page = webClient.getPage("/login");
		assertThat(page.getTitleText()).isEqualTo("Login");
	}

	@Test
	void test_bookletLogin_linkToSignup() throws FailingHttpStatusCodeException, MalformedURLException, IOException {
		final HtmlPage page = webClient.getPage("/login");
		DomNodeList<DomElement> inputs = page.getElementsByTagName("a");
		inputs.stream().forEach(domElement -> assertThat(domElement.getAttribute("href")).isEqualTo("/new-user"));
	}

	@Test
	void test_signup_title() throws FailingHttpStatusCodeException, MalformedURLException, IOException {
		final HtmlPage page = webClient.getPage("/new-user");
		assertThat(page.getTitleText()).isEqualTo("Signup");
	}

	@Test
	void test_signup_formWorksUseSWLayers() throws FailingHttpStatusCodeException, MalformedURLException, IOException {
		when(userService.existsByUsername(anyString())).thenReturn(false);
		when(studentService.addStudent(anyString(), anyString(), anyString()))
				.thenReturn(new Student("internalId", "name", "surname", "collegeId"));
		when(userService.addUser(anyString(),anyString(),anyString())).thenReturn(new User("username", "password", "studentId"));

		final HtmlPage page = webClient.getPage("/new-user");
		final HtmlForm form = page.getFormByName("signup-form");
		
		form.getInputByName("username").setValueAttribute("username");
		form.getInputByName("password").setValueAttribute("password");
		form.getInputByName("name").setValueAttribute("name");
		form.getInputByName("surname").setValueAttribute("surname");
		form.getInputByName("collegeId").setValueAttribute("collegeId");
		form.getButtonByName("btn-submit").click();
		
		InOrder inOrder = inOrder(userService, studentService);
		inOrder.verify(userService).existsByUsername("username");
		inOrder.verify(studentService).addStudent("name", "surname", "collegeId");
		inOrder.verify(userService).addUser("username", "password", "internalId");
		inOrder.verifyNoMoreInteractions();
	}
	
	@Test
	void test_signup_buttonDoNotWorkWithUsernameOutsideOfPattern() throws FailingHttpStatusCodeException, MalformedURLException, IOException {
		when(userService.existsByUsername(anyString())).thenReturn(false);
		when(studentService.addStudent(anyString(), anyString(), anyString()))
				.thenReturn(new Student("internalId", "name", "surname", "collegeId"));
		when(userService.addUser(anyString(),anyString(),anyString())).thenReturn(new User("username", "password", "studentId"));

		final HtmlPage page = webClient.getPage("/new-user");
		final HtmlForm form = page.getFormByName("signup-form");
		
		form.getInputByName("username").setValueAttribute("user-name");
		form.getInputByName("password").setValueAttribute("password");
		form.getInputByName("name").setValueAttribute("name");
		form.getInputByName("surname").setValueAttribute("surname");
		form.getInputByName("collegeId").setValueAttribute("collegeId");
		form.getButtonByName("btn-submit").click();
		
		InOrder inOrder = inOrder(userService, studentService);
		inOrder.verifyNoMoreInteractions();
	}
	
	@Test
	void test_signup_buttonDoNotWorkWithNameOutsideOfPattern() throws FailingHttpStatusCodeException, MalformedURLException, IOException {
		when(userService.existsByUsername(anyString())).thenReturn(false);
		when(studentService.addStudent(anyString(), anyString(), anyString()))
				.thenReturn(new Student("internalId", "name", "surname", "collegeId"));
		when(userService.addUser(anyString(),anyString(),anyString())).thenReturn(new User("username", "password", "studentId"));

		final HtmlPage page = webClient.getPage("/new-user");
		final HtmlForm form = page.getFormByName("signup-form");
		
		form.getInputByName("username").setValueAttribute("username");
		form.getInputByName("password").setValueAttribute("password");
		form.getInputByName("name").setValueAttribute("n@m3");
		form.getInputByName("surname").setValueAttribute("surname");
		form.getInputByName("collegeId").setValueAttribute("collegeId");
		form.getButtonByName("btn-submit").click();
		
		InOrder inOrder = inOrder(userService, studentService);
		inOrder.verifyNoMoreInteractions();
	}
	
	@Test
	void test_signup_buttonDoNotWorkWithSurameOutsideOfPattern() throws FailingHttpStatusCodeException, MalformedURLException, IOException {
		when(userService.existsByUsername(anyString())).thenReturn(false);
		when(studentService.addStudent(anyString(), anyString(), anyString()))
				.thenReturn(new Student("internalId", "name", "surname", "collegeId"));
		when(userService.addUser(anyString(),anyString(),anyString())).thenReturn(new User("username", "password", "studentId"));

		final HtmlPage page = webClient.getPage("/new-user");
		final HtmlForm form = page.getFormByName("signup-form");
		
		form.getInputByName("username").setValueAttribute("username");
		form.getInputByName("password").setValueAttribute("password");
		form.getInputByName("name").setValueAttribute("name");
		form.getInputByName("surname").setValueAttribute("surn@m3");
		form.getInputByName("collegeId").setValueAttribute("collegeId");
		form.getButtonByName("btn-submit").click();
		
		InOrder inOrder = inOrder(userService, studentService);
		inOrder.verifyNoMoreInteractions();
	}
	
	@Test
	void test_signup_buttonDoNotWorkWithCollegeIdOutsideOfPattern() throws FailingHttpStatusCodeException, MalformedURLException, IOException {
		when(userService.existsByUsername(anyString())).thenReturn(false);
		when(studentService.addStudent(anyString(), anyString(), anyString()))
				.thenReturn(new Student("internalId", "name", "surname", "collegeId"));
		when(userService.addUser(anyString(),anyString(),anyString())).thenReturn(new User("username", "password", "studentId"));

		final HtmlPage page = webClient.getPage("/new-user");
		final HtmlForm form = page.getFormByName("signup-form");
		
		form.getInputByName("username").setValueAttribute("username");
		form.getInputByName("password").setValueAttribute("password");
		form.getInputByName("name").setValueAttribute("name");
		form.getInputByName("surname").setValueAttribute("surname");
		form.getInputByName("collegeId").setValueAttribute("college-Id");
		form.getButtonByName("btn-submit").click();
		
		InOrder inOrder = inOrder(userService, studentService);
		inOrder.verifyNoMoreInteractions();
	}
	
	@Test
	void test_signup_formShowsErrorMessageForExixtingUsername() throws FailingHttpStatusCodeException, MalformedURLException, IOException {
		when(userService.existsByUsername(anyString())).thenReturn(true);

		final HtmlPage page = webClient.getPage("/new-user");
		final HtmlForm form = page.getFormByName("signup-form");
		
		form.getInputByName("username").setValueAttribute("username");
		form.getInputByName("password").setValueAttribute("password");
		form.getInputByName("name").setValueAttribute("name");
		form.getInputByName("surname").setValueAttribute("surname");
		form.getInputByName("collegeId").setValueAttribute("collegeId");
		final HtmlPage pageWithMessage = form.getButtonByName("btn-submit").click();
		
		assertThat(pageWithMessage.getElementById("message").getTextContent()).isEqualTo("Username already used");
		
		verify(userService).existsByUsername("username");
		
		
	}

}