package booklet.presentation;

import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InOrder;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import booklet.components.AuthenticationData;
import booklet.model.Exam;
import booklet.model.Student;
import booklet.model.User;
import booklet.service.StudentService;
import booklet.service.UserService;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = StudentWebController.class)
@WithMockUser(username = "username")
class StudentWebControllerTest {

	private static final String NEW_EXAM_MAPPING = "/new";
	private static final String INDEX_MAPPING = "/";
	private static final String SAVE_EXAM_MAPPING = "/save";
	private static final String DELETE_MAPPING = "/delete";

	private static final String REDIRECT_TO_INDEX_VIEW = "redirect:/";
	private static final String ADD_EXAM_VIEW = "addExam";
	private static final String INDEX_VIEW = "index";

	private static final String STUDENT_NAME = "Checco";
	private static final String STUDENT_SURNAME = "Balestrini";
	private static final String STUDENT_COLLEGE_ID = "01";
	private static final String STUDENT_INTERNAL_ID = "CheccoBalestrini01";

	private static final String STUDENT_ATTRIBUTE_NAME = "student";
	private static final String WEIGHTED_MEAN_ATTRIBUTE_NAME = "weighted_mean";
	private static final String MEAN_ATTRIBUTE_NAME = "mean";

	@MockBean
	private StudentService studentService;

	@MockBean
	private UserService userService;

	@MockBean
	private AuthenticationData authenticationData;

	@Autowired
	private MockMvc mvc;

	private Student student;

	@BeforeEach
	void setup() {
		student = new Student(STUDENT_INTERNAL_ID, STUDENT_NAME, STUDENT_SURNAME, STUDENT_COLLEGE_ID);
		student.getExamList().add(new Exam("Logic 101", "29/02/2020", 9, 26, false));
		student.getExamList().add(new Exam("Computer Science 101", "29/02/2020", 9, 18, false));
	}

	private void initLoggedUserStudentMock(Student student) {
		when(authenticationData.getAuthenticatedUsername()).thenReturn(STUDENT_NAME);
		when(userService.findUserByUsername(STUDENT_NAME))
				.thenReturn(new User("username", "password", STUDENT_INTERNAL_ID));
		when(studentService.getStudentById(STUDENT_INTERNAL_ID)).thenReturn(student);
		when(studentService.getStudentAverageById(STUDENT_INTERNAL_ID)).thenReturn(22.0f);
		when(studentService.getStudentWeightedAverageById(STUDENT_INTERNAL_ID)).thenReturn(22.0f);

	}

	@Test
	void test_index_Status200() throws Exception {
		initLoggedUserStudentMock(student);

		mvc.perform(get(INDEX_MAPPING)).andExpect(status().is2xxSuccessful());
	}

	@Test
	void test_index_ReturnHomeView() throws Exception {
		initLoggedUserStudentMock(student);

		mvc.perform(get(INDEX_MAPPING)).andExpect(view().name(INDEX_VIEW));
	}

	@Test
	void test_HomeView_ShowsStudent() throws Exception {
		initLoggedUserStudentMock(student);

		mvc.perform(get(INDEX_MAPPING)).andExpect(view().name(INDEX_VIEW))
				.andExpect(model().attribute(STUDENT_ATTRIBUTE_NAME, student));
	}

	@Test
	void test_HomeView_UseServiceLayer() throws Exception {
		initLoggedUserStudentMock(student);

		mvc.perform(get(INDEX_MAPPING));

		InOrder inOrder = Mockito.inOrder(studentService, userService, authenticationData);
		inOrder.verify(authenticationData).getAuthenticatedUsername();
		inOrder.verify(userService).findUserByUsername(STUDENT_NAME);
		inOrder.verify(studentService).getStudentAverageById(STUDENT_INTERNAL_ID);
		inOrder.verify(studentService).getStudentWeightedAverageById(STUDENT_INTERNAL_ID);
	}

	@Test
	void test_homeView_showsArithmeticMean() throws Exception {
		initLoggedUserStudentMock(student);

		mvc.perform(get(INDEX_MAPPING)).andExpect(model().attribute(MEAN_ATTRIBUTE_NAME, 22.0f));
	}

	@Test
	void test_homeView_showsWeightedMean() throws Exception {
		initLoggedUserStudentMock(student);

		mvc.perform(get(INDEX_MAPPING)).andExpect(model().attribute(WEIGHTED_MEAN_ATTRIBUTE_NAME, 22.0f));
	}

	@Test
	void test_newExam_Status200() throws Exception {
		mvc.perform(get(NEW_EXAM_MAPPING)).andExpect(status().is2xxSuccessful());
	}

	@Test
	void test_newExam_returnAddExamView() throws Exception {
		mvc.perform(get(NEW_EXAM_MAPPING)).andExpect(view().name(ADD_EXAM_VIEW));
	}

	@Test
	void test_newExam_ModelHasExamAttribute() throws Exception {
		mvc.perform(get(NEW_EXAM_MAPPING)).andExpect(view().name(ADD_EXAM_VIEW))
				.andExpect(model().attribute("exam", new Exam("", "", 0, 0, false)));
	}

	@Test
	void test_saveExam_Status300() throws Exception {
		Exam exam = new Exam("Analysis", "24/02/22", 12, 26, false);
		initLoggedUserStudentMock(student);
		student.getExamList().add(exam);
		when(studentService.addExamToStudentById(STUDENT_INTERNAL_ID, exam)).thenReturn(student);

		mvc.perform(post(SAVE_EXAM_MAPPING).param("name", exam.getName()).param("date", exam.getDate())
				.param("cfu", String.valueOf(exam.getCfu())).param("grade", String.valueOf(exam.getGrade()))
				.param("laude", String.valueOf(exam.isLaude())).with(csrf())).andExpect(status().is3xxRedirection());
	}

	@Test
	void test_saveExam_RedirectToHomeView() throws Exception {
		Exam exam = new Exam("Analysis", "24/02/22", 12, 26, false);
		initLoggedUserStudentMock(student);
		student.getExamList().add(exam);
		when(studentService.addExamToStudentById(STUDENT_INTERNAL_ID, exam)).thenReturn(student);

		mvc.perform(post(SAVE_EXAM_MAPPING).param("name", exam.getName()).param("date", exam.getDate())
				.param("cfu", String.valueOf(exam.getCfu())).param("grade", String.valueOf(exam.getGrade()))
				.param("laude", String.valueOf(exam.isLaude())).with(csrf()))
				.andExpect(view().name(REDIRECT_TO_INDEX_VIEW));
	}

	@Test
	void test_saveExam_UseServiceLayer() throws Exception {
		Exam exam = new Exam("Analysis", "24/02/22", 12, 26, false);
		initLoggedUserStudentMock(student);
		student.getExamList().add(exam);
		when(studentService.addExamToStudentById(STUDENT_INTERNAL_ID, exam)).thenReturn(student);

		mvc.perform(post(SAVE_EXAM_MAPPING).param("name", exam.getName()).param("date", exam.getDate())
				.param("cfu", String.valueOf(exam.getCfu())).param("grade", String.valueOf(exam.getGrade()))
				.param("laude", String.valueOf(exam.isLaude())).with(csrf()));

		InOrder inOrder = inOrder(studentService, userService, authenticationData);
		inOrder.verify(authenticationData).getAuthenticatedUsername();
		inOrder.verify(userService).findUserByUsername(STUDENT_NAME);
		inOrder.verify(studentService).addExamToStudentById(STUDENT_INTERNAL_ID, exam);
	}

	@Test
	void test_deleteExam_Status300() throws Exception {
		initLoggedUserStudentMock(student);
		student.getExamList().removeIf(exam -> exam.getName().equals("Logic 101"));
		when(studentService.removeExamByNameToStudentById(STUDENT_INTERNAL_ID, "Logic 101")).thenReturn(student);

		mvc.perform(get(DELETE_MAPPING + "?id=Logic 101")).andExpect(status().is3xxRedirection());
	}

	@Test
	void test_deleteExam_RedirectToHomeView() throws Exception {
		initLoggedUserStudentMock(student);
		student.getExamList().removeIf(exam -> exam.getName().equals("Logic 101"));
		when(studentService.removeExamByNameToStudentById(STUDENT_INTERNAL_ID, "Logic 101")).thenReturn(student);

		mvc.perform(get(DELETE_MAPPING + "?id=Logic 101")).andExpect(view().name(REDIRECT_TO_INDEX_VIEW));
	}

	@Test
	void test_deleteExam_UseServiceLayer() throws Exception {
		initLoggedUserStudentMock(student);
		student.getExamList().removeIf(exam -> exam.getName().equals("Logic 101"));
		when(studentService.removeExamByNameToStudentById(STUDENT_INTERNAL_ID, "Logic 101")).thenReturn(student);

		mvc.perform(get(DELETE_MAPPING + "?id=Logic 101"));
		InOrder inOrder = Mockito.inOrder(studentService, userService, authenticationData);
		inOrder.verify(authenticationData).getAuthenticatedUsername();
		inOrder.verify(userService).findUserByUsername(STUDENT_NAME);
		inOrder.verify(studentService).removeExamByNameToStudentById(STUDENT_INTERNAL_ID, "Logic 101");
	}

}
