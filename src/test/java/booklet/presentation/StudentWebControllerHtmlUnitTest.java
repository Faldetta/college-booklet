package booklet.presentation;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.net.MalformedURLException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;

import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlElement;
import com.gargoylesoftware.htmlunit.html.HtmlForm;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlTable;

import booklet.components.AuthenticationData;
import booklet.model.Exam;
import booklet.model.Student;
import booklet.model.User;
import booklet.service.StudentService;
import booklet.service.UserService;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = StudentWebController.class)
@WithMockUser(username = "username")
class StudentWebControllerHtmlUnitTest {

	@MockBean
	private StudentService studentService;

	@MockBean
	private UserService userService;

	@MockBean
	private AuthenticationData authenticationData;

	@Autowired
	private WebClient webClient;

	private final String STUDENT_NAME = "Checco";
	private final String STUDENT_SURNAME = "Balestrini";
	private final String STUDENT_COLLEGE_ID = "01";
	private final String STUDENT_INTERNAL_ID = "CheccoBalestrini01";

	private Student student;

	@BeforeEach
	void setup() {
		student = new Student(STUDENT_INTERNAL_ID, STUDENT_NAME, STUDENT_SURNAME, STUDENT_COLLEGE_ID);
		student.getExamList().add(new Exam("Logic 101", "29/02/2020", 9, 26, false));
		student.getExamList().add(new Exam("Computer Science 101", "29/02/2020", 6, 18, false));
	}

	private void initLoggedUserStudentMock(Student student) {
		when(authenticationData.getAuthenticatedUsername()).thenReturn(STUDENT_NAME);
		when(userService.findUserByUsername(STUDENT_NAME))
				.thenReturn(new User("username", "password", STUDENT_INTERNAL_ID));
		when(studentService.getStudentById(STUDENT_INTERNAL_ID)).thenReturn(student);
		when(studentService.getStudentAverageById(STUDENT_INTERNAL_ID)).thenReturn(22.0f);
		when(studentService.getStudentWeightedAverageById(STUDENT_INTERNAL_ID)).thenReturn(22.8f);
	}

	@Test
	void test_homePageTitle() throws Exception {
		initLoggedUserStudentMock(student);
		HtmlPage page = webClient.getPage("/");
		assertThat(page.getTitleText()).isEqualTo("College Booklet");
	}

	@Test
	void test_nameSurnameCollegeIDStudent() throws Exception {
		initLoggedUserStudentMock(student);
		HtmlPage page = webClient.getPage("/");
		HtmlElement elementName = page.getHtmlElementById("studentName");
		assertThat(elementName.asText()).isEqualTo("Checco");
		HtmlElement elementSurname = page.getHtmlElementById("studentSurname");
		assertThat(elementSurname.asText()).isEqualTo("Balestrini");
		HtmlElement elementCollegeId = page.getHtmlElementById("studentCollegeId");
		assertThat(elementCollegeId.asText()).isEqualTo("01");
	}

	@Test
	void test_emptyTableExams() throws Exception {
		Student studentTest = new Student(STUDENT_INTERNAL_ID, STUDENT_NAME, STUDENT_SURNAME, STUDENT_COLLEGE_ID);
		initLoggedUserStudentMock(studentTest);
		HtmlPage page = webClient.getPage("/");
		HtmlElement element = page.getHtmlElementById("noExam");
		assertThat(element.asText()).isEqualTo("No exams");
	}

	@Test
	void test_tableExamsWithExams() throws Exception {
		initLoggedUserStudentMock(student);
		HtmlPage page = webClient.getPage("/");
		HtmlTable table = page.getHtmlElementById("student.examList_table");
		assertThat(table.asText()).isEqualTo("My exams\n" + "Name	Date	CFU	Grade	Laude\n"
				+ "Logic 101	29/02/2020	9	26	false	Delete\n"
				+ "Computer Science 101	29/02/2020	6	18	false	Delete");
	}

	@Test
	void test_meanAndWeightedMean() throws Exception {
		initLoggedUserStudentMock(student);
		HtmlPage page = webClient.getPage("/");
		HtmlElement elementMean = page.getHtmlElementById("meanId");
		assertThat(elementMean.asText()).isEqualTo("22.0");
		HtmlElement elementWeightedMean = page.getHtmlElementById("weightedMeanId");
		assertThat(elementWeightedMean.asText()).isEqualTo("22.8");
	}

	@Test
	void test_addExamButton_linksToNewExamPage()
			throws FailingHttpStatusCodeException, MalformedURLException, IOException {
		initLoggedUserStudentMock(student);
		HtmlPage page = webClient.getPage("/");
		HtmlElement element = page.getHtmlElementById("new");
		assertThat(element.getAttribute("href")).isEqualTo("/new");
	}

	@Test
	void test_logoutLink_linksToLogout() throws FailingHttpStatusCodeException, MalformedURLException, IOException {
		initLoggedUserStudentMock(student);
		HtmlPage page = webClient.getPage("/");
		HtmlElement element = page.getHtmlElementById("logout");
		assertThat(element.getAttribute("href")).isEqualTo("/logout");
	}

	@Test
	void test_addExamTitle() throws Exception {
		initLoggedUserStudentMock(student);
		HtmlPage page = webClient.getPage("/new");
		assertThat(page.getTitleText()).isEqualTo("Add Exam");
	}

	@Test
	void test_editFormForExam() throws Exception {
		Exam exam = new Exam("Logic 101", "29/02/2020", 9, 26, false);
		when(studentService.addExamToStudentById(any(String.class), any(Exam.class))).thenReturn(student);
		initLoggedUserStudentMock(student);

		HtmlPage page = this.webClient.getPage("/new");
		final HtmlForm form = page.getFormByName("exam_form");

		form.getInputByName("name").setValueAttribute("Logic 101");
		form.getInputByName("date").setValueAttribute("29/02/2020");
		form.getInputByName("cfu").setValueAttribute("9");
		form.getInputByName("grade").setValueAttribute("26");
		form.getInputByName("laude").setValueAttribute("false");
		form.getButtonByName("btn_submit").click();

		verify(studentService).addExamToStudentById(STUDENT_INTERNAL_ID, exam);
	}

}
