package booklet.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.NoSuchElementException;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.password.PasswordEncoder;

import booklet.model.User;
import booklet.persistence.UserRepository;

@ExtendWith(MockitoExtension.class)
class UserServiceTest {

	private static final String ENCODED_PASSWORD = "encodedPassword";
	private static final String USERNAME = "username";
	private static final String PASSWORD = "password";
	private static final String STUDENT_ID = "studentId";

	@Mock
	private UserRepository userRepository;

	@Mock
	private PasswordEncoder passwordEncoder;;

	@InjectMocks
	private UserService userService;

	@BeforeEach
	void setup() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	void test_addUser_callsPasswordEncoderMethod() {
		when(passwordEncoder.encode(anyString())).thenReturn(ENCODED_PASSWORD);
		when(userRepository.save(any(User.class))).thenReturn(new User(USERNAME, ENCODED_PASSWORD, STUDENT_ID));

		userService.addUser(USERNAME, PASSWORD, STUDENT_ID);

		verify(passwordEncoder).encode(PASSWORD);
	}

	@Test
	void test_addUser_callsUserRepositoryMethod() {
		when(passwordEncoder.encode(anyString())).thenReturn(ENCODED_PASSWORD);
		when(userRepository.save(any(User.class))).thenReturn(new User(USERNAME, ENCODED_PASSWORD, STUDENT_ID));

		userService.addUser(USERNAME, PASSWORD, STUDENT_ID);

		verify(userRepository).save(new User(USERNAME, ENCODED_PASSWORD, STUDENT_ID));
	}

	@Test
	void test_addUser_mocksAreCalledInTheRightOrder() {
		when(passwordEncoder.encode(anyString())).thenReturn(ENCODED_PASSWORD);
		when(userRepository.save(any(User.class))).thenReturn(new User(USERNAME, ENCODED_PASSWORD, STUDENT_ID));

		userService.addUser(USERNAME, PASSWORD, STUDENT_ID);

		InOrder inOrder = Mockito.inOrder(passwordEncoder, userRepository);
		inOrder.verify(passwordEncoder).encode(PASSWORD);
		inOrder.verify(userRepository).save(new User(USERNAME, ENCODED_PASSWORD, STUDENT_ID));
	}

	@Test
	void test_addUser_returnsTheRightUser() {
		when(passwordEncoder.encode(anyString())).thenReturn(ENCODED_PASSWORD);
		when(userRepository.save(any(User.class))).thenReturn(new User(USERNAME, ENCODED_PASSWORD, STUDENT_ID));

		assertThat(userService.addUser(USERNAME, PASSWORD, STUDENT_ID))
				.isEqualTo(new User(USERNAME, ENCODED_PASSWORD, STUDENT_ID));
	}

	@Test
	void test_findUserByUsername_callsUserRepositoryMethod() {
		when(userRepository.findById(anyString()))
				.thenReturn(Optional.of(new User(USERNAME, ENCODED_PASSWORD, STUDENT_ID)));

		userService.findUserByUsername(USERNAME);

		verify(userRepository).findById(USERNAME);
	}

	@Test
	void test_findUserByUsername_throwsExceptionWhenNoUserIsFound() {
		when(userRepository.findById(anyString())).thenReturn(Optional.empty());

		assertThatThrownBy(() -> userService.findUserByUsername(USERNAME)).isInstanceOf(NoSuchElementException.class);
	}

	@Test
	void test_findUserByUsername_returnAStudentWithTheRightUsername() {
		when(userRepository.findById(anyString()))
				.thenReturn(Optional.of(new User(USERNAME, ENCODED_PASSWORD, STUDENT_ID)));

		assertThat(userService.findUserByUsername(USERNAME)).isOfAnyClassIn(User.class);

	}

	@Test
	void test_existsByUsername_callsUserRepositoryMethod() {
		when(userRepository.existsById(anyString())).thenReturn(false);

		userService.existsByUsername(USERNAME);

		verify(userRepository).existsById(USERNAME);
	}

	@Test
	void test_existsByUsername_returnsSameAsRepositoryMethod() {
		when(userRepository.existsById(anyString())).thenReturn(true).thenReturn(false);

		assertThat(userService.existsByUsername(USERNAME)).isTrue();
		assertThat(userService.existsByUsername(USERNAME)).isFalse();
	}

}
