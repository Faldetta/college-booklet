package booklet.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import booklet.model.User;
import booklet.persistence.UserRepository;

@ExtendWith(MockitoExtension.class)
class BookletUserDetailsServiceTest {

	private static final String STUDENT_ID = "studentId";
	private static final String PASSWORD = "password";
	private static final String USERNAME = "username";
	private static final String STUDENT_ROLE = "STUDENT";

	@Mock
	private UserRepository userRepository;

	@InjectMocks
	private BookletUserDetailsService userDetailService;

	@BeforeEach
	void setup() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	void test_loadByUsername_produceRightUserDetail() {
		Collection<GrantedAuthority> grantedAuthorities = new ArrayList<>();
		grantedAuthorities.add(new SimpleGrantedAuthority(STUDENT_ROLE));

		when(userRepository.findById(USERNAME)).thenReturn(Optional.of(new User(USERNAME, PASSWORD, STUDENT_ID)));

		assertThat(userDetailService.loadUserByUsername(USERNAME)).isEqualTo(
				new org.springframework.security.core.userdetails.User(USERNAME, PASSWORD, grantedAuthorities));
	}

	@Test
	void test_loadByUsername_produceRightUserRoles() {
		Collection<GrantedAuthority> grantedAuthorities = new ArrayList<>();
		grantedAuthorities.add(new SimpleGrantedAuthority(STUDENT_ROLE));

		when(userRepository.findById(USERNAME)).thenReturn(Optional.of(new User(USERNAME, PASSWORD, STUDENT_ID)));

		assertThat(userDetailService.loadUserByUsername(USERNAME).getAuthorities().toArray())
				.isEqualTo(grantedAuthorities.toArray());
	}

	@Test
	void test_loadByUsername_usePersistenceLayer() {
		Collection<GrantedAuthority> grantedAuthorities = new ArrayList<>();
		grantedAuthorities.add(new SimpleGrantedAuthority(STUDENT_ROLE));

		when(userRepository.findById(USERNAME)).thenReturn(Optional.of(new User(USERNAME, PASSWORD, STUDENT_ID)));

		userDetailService.loadUserByUsername(USERNAME);
		verify(userRepository).findById(USERNAME);
	}

	@Test
	void test_loadByUsername_throwsExceptionWhenNoUserIsFound() {
		Collection<GrantedAuthority> grantedAuthorities = new ArrayList<>();
		grantedAuthorities.add(new SimpleGrantedAuthority(STUDENT_ROLE));

		when(userRepository.findById(USERNAME)).thenReturn(Optional.empty());

		assertThatThrownBy(() -> userDetailService.loadUserByUsername(USERNAME))
				.hasMessage(String.format("Username '%s' not found", USERNAME))
				.isInstanceOf(UsernameNotFoundException.class);
	}

}
