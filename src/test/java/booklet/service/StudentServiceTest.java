package booklet.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import java.util.NoSuchElementException;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import booklet.model.Exam;
import booklet.model.Student;
import booklet.persistence.StudentRepository;

@ExtendWith(MockitoExtension.class)
class StudentServiceTest {

	private final String STUDENT_NAME = "Checco";
	private final String STUDENT_SURNAME = "Balestrini";
	private final String STUDENT_COLLEGE_ID = "01";
	private final String STUDENT_INTERNAL_ID = "CheccoBalestrini01";

	@Mock
	private StudentRepository studentRepository;

	@InjectMocks
	private StudentService studentService;

	@BeforeEach
	public void setup() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	void test_addStudent_callsSave() {
		Student student = new Student(STUDENT_INTERNAL_ID, STUDENT_NAME, STUDENT_SURNAME, STUDENT_COLLEGE_ID);
		when(studentRepository.save(any(Student.class))).thenReturn(student);

		studentService.addStudent(STUDENT_NAME, STUDENT_SURNAME, STUDENT_COLLEGE_ID);

		Mockito.verify(studentRepository).save(student);

	}

	@Test
	void test_addStudent_manageIdConflictWithIncreasingNumbers() {
		when(studentRepository.existsById(anyString())).thenReturn(false).thenReturn(true, false).thenReturn(true, true,
				false);

		when(studentRepository.save(any(Student.class)))
				.thenReturn(new Student(STUDENT_INTERNAL_ID, STUDENT_NAME, STUDENT_SURNAME, STUDENT_COLLEGE_ID))
				.thenReturn(new Student("CheccoBalestrini01-1", STUDENT_NAME, STUDENT_SURNAME, STUDENT_COLLEGE_ID))
				.thenReturn(new Student("CheccoBalestrini01-2", STUDENT_NAME, STUDENT_SURNAME, STUDENT_COLLEGE_ID));

		studentService.addStudent(STUDENT_NAME, STUDENT_SURNAME, STUDENT_COLLEGE_ID);

		assertThat(studentService.addStudent(STUDENT_NAME, STUDENT_SURNAME, STUDENT_COLLEGE_ID).getId())
				.isEqualTo("CheccoBalestrini01-1");
		assertThat(studentService.addStudent(STUDENT_NAME, STUDENT_SURNAME, STUDENT_COLLEGE_ID).getId())
				.isEqualTo("CheccoBalestrini01-2");

	}

	@Test
	void test_getStudent_callsFindByIdMethodWithTheSameId() {
		Student student = new Student(STUDENT_INTERNAL_ID, STUDENT_NAME, STUDENT_SURNAME, STUDENT_COLLEGE_ID);

		when(studentRepository.findById(anyString())).thenReturn(Optional.of((student)));

		studentService.getStudentById(STUDENT_INTERNAL_ID);

		Mockito.verify(studentRepository).findById(STUDENT_INTERNAL_ID);

	}

	@Test
	void test_getStudent_throwsExeptionWhenObtainEmpyOptional() {
		when(studentRepository.findById(anyString())).thenReturn(Optional.empty());

		assertThatThrownBy(() -> studentService.getStudentById(STUDENT_INTERNAL_ID))
				.isInstanceOf(NoSuchElementException.class);
	}

	@Test
	void test_getStudentWeightedAverageById_isZeroWithNoExams() {
		Student student = new Student(STUDENT_INTERNAL_ID, STUDENT_NAME, STUDENT_SURNAME, STUDENT_COLLEGE_ID);

		when(studentRepository.findById(anyString())).thenReturn(Optional.of((student)));

		assertThat(studentService.getStudentWeightedAverageById(STUDENT_INTERNAL_ID)).isZero();
	}

	@Test
	void test_getStudentWeightedAverageById_isTheExamGradeWithOneExam() {
		Student student = new Student(STUDENT_INTERNAL_ID, STUDENT_NAME, STUDENT_SURNAME, STUDENT_COLLEGE_ID);
		student.getExamList().add(new Exam("Logic 101", "29/02/2020", 9, 26, false));

		when(studentRepository.findById(anyString())).thenReturn(Optional.of((student)));

		assertThat(studentService.getStudentWeightedAverageById(STUDENT_INTERNAL_ID)).isEqualTo(26f);
	}

	@Test
	void test_getStudentWeightedAverageById_isTheWeightedAverageWithTwoExams() {
		Student student = new Student(STUDENT_INTERNAL_ID, STUDENT_NAME, STUDENT_SURNAME, STUDENT_COLLEGE_ID);
		student.getExamList().add(new Exam("Logic 101", "29/02/2020", 9, 26, false));
		student.getExamList().add(new Exam("Computer Science 101", "29/02/2020", 12, 18, false));

		when(studentRepository.findById(anyString())).thenReturn(Optional.of((student)));

		assertThat(studentService.getStudentWeightedAverageById(STUDENT_INTERNAL_ID)).isBetween(21.42f, 21.43f);
	}

	@Test
	void test_getStudentWeightedAverageById_returnsZeroWhenTotalCFUIsZero() {
		Student student = new Student(STUDENT_INTERNAL_ID, STUDENT_NAME, STUDENT_SURNAME, STUDENT_COLLEGE_ID);
		student.getExamList().add(new Exam("Logic 101", "29/02/2020", 9, 26, false));
		student.getExamList().add(new Exam("Computer Science 101", "29/02/2020", -9, 18, false));

		when(studentRepository.findById(anyString())).thenReturn(Optional.of((student)));

		assertThat(studentService.getStudentWeightedAverageById(STUDENT_INTERNAL_ID)).isZero();

	}

	@Test
	void test_getStudentAverageById_isZeroWithNoExams() {
		Student student = new Student(STUDENT_INTERNAL_ID, STUDENT_NAME, STUDENT_SURNAME, STUDENT_COLLEGE_ID);

		when(studentRepository.findById(anyString())).thenReturn(Optional.of((student)));

		assertThat(studentService.getStudentAverageById(STUDENT_INTERNAL_ID)).isZero();
	}

	@Test
	void test_getStudentAverageById_isTheExamGradeWithOneExam() {
		Student student = new Student(STUDENT_INTERNAL_ID, STUDENT_NAME, STUDENT_SURNAME, STUDENT_COLLEGE_ID);
		student.getExamList().add(new Exam("Logic 101", "29/02/2020", 9, 26, false));

		when(studentRepository.findById(anyString())).thenReturn(Optional.of((student)));

		assertThat(studentService.getStudentAverageById(STUDENT_INTERNAL_ID)).isEqualTo(26f);
	}

	@Test
	void test_getStudentAverageById_isTheAverageWithTwoExams() {
		Student student = new Student(STUDENT_INTERNAL_ID, STUDENT_NAME, STUDENT_SURNAME, STUDENT_COLLEGE_ID);
		student.getExamList().add(new Exam("Logic 101", "29/02/2020", 9, 26, false));
		student.getExamList().add(new Exam("Computer Science 101", "29/02/2020", 12, 18, false));

		when(studentRepository.findById(anyString())).thenReturn(Optional.of((student)));

		assertThat(studentService.getStudentAverageById(STUDENT_INTERNAL_ID)).isEqualTo(22f);
	}

	@Test
	void test_addExamToStudentById_callsFindByIdAndSave() {
		Student student = new Student(STUDENT_INTERNAL_ID, STUDENT_NAME, STUDENT_SURNAME, STUDENT_COLLEGE_ID);
		Student studentWithExam = new Student(STUDENT_INTERNAL_ID, STUDENT_NAME, STUDENT_SURNAME, STUDENT_COLLEGE_ID);
		studentWithExam.getExamList().add(new Exam("Logic 101", "29/02/2020", 9, 26, false));

		when(studentRepository.findById(anyString())).thenReturn(Optional.of((student)));
		when(studentRepository.save(any(Student.class))).thenReturn(studentWithExam);

		studentService.addExamToStudentById(STUDENT_INTERNAL_ID, new Exam("Logic 101", "29/02/2020", 9, 26, false));

		InOrder inOrder = Mockito.inOrder(studentRepository);
		inOrder.verify(studentRepository).findById(STUDENT_INTERNAL_ID);
		inOrder.verify(studentRepository).save(studentWithExam);
	}

	@Test
	void test_addExamToStudentById_examIsAddedToStudent() {
		Student student = new Student(STUDENT_INTERNAL_ID, STUDENT_NAME, STUDENT_SURNAME, STUDENT_COLLEGE_ID);
		Student studentWithExam = new Student(STUDENT_INTERNAL_ID, STUDENT_NAME, STUDENT_SURNAME, STUDENT_COLLEGE_ID);
		studentWithExam.getExamList().add(new Exam("Logic 101", "29/02/2020", 9, 26, false));

		when(studentRepository.findById(anyString())).thenReturn(Optional.of((student)));
		when(studentRepository.save(any(Student.class))).thenReturn(studentWithExam);

		assertThat(studentService
				.addExamToStudentById(STUDENT_INTERNAL_ID, new Exam("Logic 101", "29/02/2020", 9, 26, false))
				.getExamList()).containsOnly(new Exam("Logic 101", "29/02/2020", 9, 26, false));
	}

	@Test
	void test_removeStudentById_callsDeleteByIdWithTheSameId() {
		doNothing().when(studentRepository).deleteById(anyString());

		studentService.removeStudentById(STUDENT_INTERNAL_ID);

		Mockito.verify(studentRepository).deleteById(STUDENT_INTERNAL_ID);
	}

	@Test
	void test_removeExamByNameToStudentById_callsFindByIdAndSave() {
		Student student = new Student(STUDENT_INTERNAL_ID, STUDENT_NAME, STUDENT_SURNAME, STUDENT_COLLEGE_ID);
		student.getExamList().add(new Exam("Logic 101", "29/02/2020", 9, 26, false));
		Student studentWithoutExam = new Student(STUDENT_INTERNAL_ID, STUDENT_NAME, STUDENT_SURNAME,
				STUDENT_COLLEGE_ID);

		when(studentRepository.findById(anyString())).thenReturn(Optional.of((student)));
		when(studentRepository.save(any(Student.class))).thenReturn(studentWithoutExam);

		studentService.removeExamByNameToStudentById(STUDENT_INTERNAL_ID, "Logic 101");

		InOrder inOrder = Mockito.inOrder(studentRepository);
		inOrder.verify(studentRepository).findById(STUDENT_INTERNAL_ID);
		inOrder.verify(studentRepository).save(studentWithoutExam);
	}

	@Test
	void test_removeExamByNameToStudentById_ExamIsRemovedToStudent() {
		Student student = new Student(STUDENT_INTERNAL_ID, STUDENT_NAME, STUDENT_SURNAME, STUDENT_COLLEGE_ID);
		student.getExamList().add(new Exam("Logic 101", "29/02/2020", 9, 26, false));
		Student studentWithoutExam = new Student(STUDENT_INTERNAL_ID, STUDENT_NAME, STUDENT_SURNAME,
				STUDENT_COLLEGE_ID);

		when(studentRepository.findById(anyString())).thenReturn(Optional.of((student)));
		when(studentRepository.save(any(Student.class))).thenReturn(studentWithoutExam);

		assertThat(studentService.removeExamByNameToStudentById(STUDENT_INTERNAL_ID, "Logic 101"))
				.isEqualTo(studentWithoutExam);
	}

	@Test
	void test_removeExamByNameToStudentById_ExamWithDifferentNameIsNotRemovedToStudent() {
		Student student = new Student(STUDENT_INTERNAL_ID, STUDENT_NAME, STUDENT_SURNAME, STUDENT_COLLEGE_ID);
		student.getExamList().add(new Exam("Logic 101", "29/02/2020", 9, 26, false));
		Student studentWithoutExam = new Student(STUDENT_INTERNAL_ID, STUDENT_NAME, STUDENT_SURNAME,
				STUDENT_COLLEGE_ID);

		when(studentRepository.findById(anyString())).thenReturn(Optional.of((student)));
		when(studentRepository.save(any(Student.class))).thenReturn(student);

		assertThat(studentService.removeExamByNameToStudentById(STUDENT_INTERNAL_ID, "Logic 102"))
				.isNotEqualTo(studentWithoutExam);
	}

}
