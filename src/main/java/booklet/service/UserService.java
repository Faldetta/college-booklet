package booklet.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import booklet.model.User;
import booklet.persistence.UserRepository;

@Service
public class UserService {

	private UserRepository userRepository;
	private PasswordEncoder passwordEncoder;

	@Autowired
	public UserService(UserRepository userRepository, PasswordEncoder passwordEncoder) {
		this.userRepository = userRepository;
		this.passwordEncoder = passwordEncoder;
	}

	public User addUser(String username, String password, String studentId) {
		var user = new User(username, passwordEncoder.encode(password), studentId);
		userRepository.save(user);
		return user;
	}

	public User findUserByUsername(String username) {
		return userRepository.findById(username).orElseThrow();
	}

	public boolean existsByUsername(String username) {
		return userRepository.existsById(username);
	}

}
