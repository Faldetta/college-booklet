package booklet.service;

import java.util.Collection;

import org.springframework.stereotype.Service;

import booklet.model.Exam;
import booklet.model.Student;
import booklet.persistence.StudentRepository;

@Service
public class StudentService {

	private StudentRepository studentRepository;

	public StudentService(StudentRepository studentRepository) {
		this.studentRepository = studentRepository;
	}

	public Student addStudent(String name, String surname, String collegeId) {
		String baseId = name + surname + collegeId;
		var studentId = String.valueOf(baseId);
		var count = 0;
		while (studentRepository.existsById(studentId)) {
			studentId = String.valueOf(baseId);
			count = count + 1;
			studentId = studentId.concat("-").concat(String.valueOf(count));
		}
		var toSave = new Student(studentId, name, surname, collegeId);
		studentRepository.save(toSave);
		return toSave;
	}

	public Student getStudentById(String studentId) {
		return studentRepository.findById(studentId).orElseThrow();
	}

	public float getStudentWeightedAverageById(String studentId) {
		Collection<Exam> examList = getStudentById(studentId).getExamList();
		float average = 0;
		if (!examList.isEmpty()) {
			var totalCFU = 0;
			for (Exam exam : examList) {
				int cfu = exam.getCfu();
				average = average + cfu * exam.getGrade();
				totalCFU = totalCFU + cfu;
			}
			if (totalCFU != 0) {
				average = average / totalCFU;
			} else {
				average = 0;
			}
		}
		return average;
	}

	public float getStudentAverageById(String studentId) {
		Collection<Exam> examList = getStudentById(studentId).getExamList();
		float average = 0;
		if (!examList.isEmpty()) {
			for (Exam exam : examList) {
				average = average + exam.getGrade();
			}
			average = average / examList.size();
		}
		return average;
	}

	public Student addExamToStudentById(String studentId, Exam exam) {
		var student = getStudentById(studentId);
		student.getExamList().add(exam);
		studentRepository.save(student);
		return student;
	}

	public void removeStudentById(String studentId) {
		studentRepository.deleteById(studentId);
	}

	public Student removeExamByNameToStudentById(String studentId, String examName) {
		var student = getStudentById(studentId);
		student.getExamList().removeIf(exam -> exam.getName().equals(examName));
		studentRepository.save(student);
		return student;
	}

}
