package booklet.model;

import java.util.Objects;

public class UserDTO {

	private String username;
	private String password;
	private String studentId;

	public UserDTO() {
		super();
	}

	public UserDTO(String username, String password, String studentId) {
		this.username = username;
		this.password = password;
		this.studentId = studentId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getStudentId() {
		return studentId;
	}

	public void setStudentId(String studentId) {
		this.studentId = studentId;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		var user = (UserDTO) o;
		return Objects.equals(username, user.username) && Objects.equals(password, user.password)
				&& Objects.equals(studentId, user.studentId);
	}

	@Override
	public int hashCode() {
		return Objects.hash(username, password, studentId);
	}
	
}
