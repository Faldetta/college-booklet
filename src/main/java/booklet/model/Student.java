package booklet.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document("Students")
public class Student {

	@Id
	private String id;

	private String name;
	private String surname;
	private String collegeId;
	private Collection<Exam> examList;

	public Student(String id, String name, String surname, String collegeId) {
		this.id = id;
		this.name = name;
		this.surname = surname;
		this.collegeId = collegeId;
		examList = new ArrayList<>();
	}

	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getSurname() {
		return surname;
	}

	public String getCollegeId() {
		return collegeId;
	}

	public Collection<Exam> getExamList() {
		return examList;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		var student = (Student) o;
		return Objects.equals(id, student.id) && Objects.equals(name, student.name)
				&& Objects.equals(surname, student.surname) && Objects.equals(collegeId, student.collegeId)
				&& Objects.equals(examList, student.examList);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, name, surname, collegeId, examList);
	}

	@Override
	public String toString() {
		return "Student{" + "id='" + id + '\'' + ", name='" + name + '\'' + ", surname='" + surname + '\''
				+ ", collegeId='" + collegeId + '\'' + ", examList=" + examList + '}';
	}

}
