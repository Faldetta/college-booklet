package booklet.model;

import java.util.Objects;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document("Users")
public class User {

	@Id
	private String username;

	private String password;
	private String studentId;

	public User(String username, String password, String studentId) {
		this.username = username;
		this.password = password;
		this.studentId = studentId;
	}

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

	public String getStudentId() {
		return studentId;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		var user = (User) o;
		return Objects.equals(username, user.username) && Objects.equals(password, user.password)
				&& Objects.equals(studentId, user.studentId);
	}

	@Override
	public int hashCode() {
		return Objects.hash(username, password, studentId);
	}

}
