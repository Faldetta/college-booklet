package booklet.model;

import java.util.Collection;
import java.util.Objects;

public class StudentDTO {

	private String id;
	private String name;
	private String surname;
	private String collegeId;
	private Collection<Exam> examList;

	public StudentDTO() {
		super();
	}

	public StudentDTO(String id, String name, String surname, String collegeId, Collection<Exam> examList) {
		this.id = id;
		this.name = name;
		this.surname = surname;
		this.collegeId = collegeId;
		this.examList = examList;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getCollegeId() {
		return collegeId;
	}

	public void setCollegeId(String collegeId) {
		this.collegeId = collegeId;
	}

	public Collection<Exam> getExamList() {
		return examList;
	}

	public void setExamList(Collection<Exam> examList) {
		this.examList = examList;
	}
	
	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		var student = (StudentDTO) o;
		return Objects.equals(id, student.id) && Objects.equals(name, student.name)
				&& Objects.equals(surname, student.surname) && Objects.equals(collegeId, student.collegeId)
				&& Objects.equals(examList, student.examList);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, name, surname, collegeId, examList);
	}

}
