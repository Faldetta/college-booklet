package booklet.model;

import java.util.Objects;

public class Exam {

	private String name;
	private String date;
	private int cfu;
	private int grade;
	private boolean laude;

	public Exam(String name, String date, int cfu, int grade, boolean laude) {
		this.name = name;
		this.date = date;
		this.cfu = cfu;
		this.grade = grade;
		this.laude = laude;
	}

	public String getName() {
		return name;
	}

	public String getDate() {
		return date;
	}

	public int getCfu() {
		return cfu;
	}

	public int getGrade() {
		return grade;
	}

	public boolean isLaude() {
		return laude;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		var exam = (Exam) o;
		return cfu == exam.cfu && grade == exam.grade && laude == exam.laude && Objects.equals(name, exam.name)
				&& Objects.equals(date, exam.date);
	}

	@Override
	public int hashCode() {
		return Objects.hash(name, date, cfu, grade, laude);
	}

	@Override
	public String toString() {
		return "Exam{" + "name='" + name + '\'' + ", date='" + date + '\'' + ", cfu=" + cfu + ", grade=" + grade
				+ ", laude=" + laude + '}';
	}

}
