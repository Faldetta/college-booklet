package booklet.persistence;

import java.util.Optional;

import booklet.model.User;

public interface UserRepository {

	Optional<User> findById(String id);

	User save(User user);

	boolean existsById(String id);

	void deleteAll();

}
