package booklet.persistence;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import booklet.model.Student;

@Repository
public interface MongoStudentRepository extends StudentRepository, MongoRepository<Student, Long> {

}
