package booklet.persistence;

import java.util.Optional;

import booklet.model.Student;

public interface StudentRepository {

	Student save(Student student);

	boolean existsById(String id);

	Optional<Student> findById(String id);

	void deleteById(String id);

	void deleteAll();

}
