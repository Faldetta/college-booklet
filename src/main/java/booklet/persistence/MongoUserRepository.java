package booklet.persistence;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import booklet.model.User;

@Repository
public interface MongoUserRepository extends UserRepository, MongoRepository<User, String> {

}
