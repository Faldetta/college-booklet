package booklet.components;

public interface AuthenticationData {

	public String getAuthenticatedUsername();

}
