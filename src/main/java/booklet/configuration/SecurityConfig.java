package booklet.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests().antMatchers("/new-user", "/save-user").permitAll()
				.antMatchers("/", "/new", "/save", "/delete").authenticated().and().formLogin().loginPage("/login")
				.passwordParameter("password").usernameParameter("username").and().logout().logoutUrl("/logout")
				.logoutRequestMatcher(new AntPathRequestMatcher("/logout", "GET")).clearAuthentication(true)
				.invalidateHttpSession(true).logoutSuccessUrl("/login");
	}

}
