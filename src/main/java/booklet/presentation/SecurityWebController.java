package booklet.presentation;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import booklet.model.StudentDTO;
import booklet.model.UserDTO;
import booklet.service.StudentService;
import booklet.service.UserService;

@Controller
public class SecurityWebController {

	private static final String LOGIN_PAGE = "bookletLogin";
	private static final String SIGNUP_PAGE = "signup";

	private static final String MESSAGE_ATTRIBUTE_NAME = "message";
	private static final String STUDENT_ATTRIBUTE_NAME = "student";
	private static final String USER_ATTRIBUTE_NAME = "user";

	private UserService userService;
	private StudentService studentService;

	public SecurityWebController(UserService userService, StudentService studentService) {
		this.userService = userService;
		this.studentService = studentService;
	}

	@GetMapping("/login")
	public String login() {
		return LOGIN_PAGE;
	}

	@GetMapping("/new-user")
	public String newUser(Model model) {
		model.addAttribute(USER_ATTRIBUTE_NAME, new UserDTO()).addAttribute(STUDENT_ATTRIBUTE_NAME, new StudentDTO())
				.addAttribute(MESSAGE_ATTRIBUTE_NAME, "");
		return SIGNUP_PAGE;
	}

	@PostMapping("/save-user")
	public String saveUser(Model model, UserDTO user, StudentDTO student) {
		if (userService.existsByUsername(user.getUsername())) {
			model.addAttribute(USER_ATTRIBUTE_NAME, new UserDTO())
					.addAttribute(STUDENT_ATTRIBUTE_NAME, new StudentDTO())
					.addAttribute(MESSAGE_ATTRIBUTE_NAME, "Username already used");
			return SIGNUP_PAGE;
		}
		userService.addUser(user.getUsername(), user.getPassword(),
				studentService.addStudent(student.getName(), student.getSurname(), student.getCollegeId()).getId());
		return LOGIN_PAGE;
	}

}
