package booklet.presentation;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import booklet.components.AuthenticationData;
import booklet.model.Exam;
import booklet.service.StudentService;
import booklet.service.UserService;

@Controller
public class StudentWebController {

	private static final String EXAM_ATTRIBUTE_NAME = "exam";
	private static final String STUDENT_ATTRIBUTE_NAME = "student";
	private static final String WEIGHTED_MEAN_ATTRIBUTE_NAME = "weighted_mean";
	private static final String MEAN_ATTRIBUTE_NAME = "mean";

	private static final String INDEX_PAGE = "index";
	private static final String ADD_EXAM_PAGE = "addExam";
	private static final String REDIRECT_TO_INDEX_PAGE = "redirect:/";

	private StudentService studentService;
	private UserService userService;
	private AuthenticationData authenticationData;

	public StudentWebController(StudentService studentService, UserService userService,
			AuthenticationData authenticationData) {
		this.studentService = studentService;
		this.userService = userService;
		this.authenticationData = authenticationData;
	}

	private String getLoggedStudentId() {
		return userService.findUserByUsername(authenticationData.getAuthenticatedUsername()).getStudentId();
	}

	@GetMapping("/")
	public String index(Model model) {
		String loggedStudentId = getLoggedStudentId();
		model.addAttribute(STUDENT_ATTRIBUTE_NAME, studentService.getStudentById(loggedStudentId))
				.addAttribute(MEAN_ATTRIBUTE_NAME, studentService.getStudentAverageById(loggedStudentId)).addAttribute(
						WEIGHTED_MEAN_ATTRIBUTE_NAME, studentService.getStudentWeightedAverageById(loggedStudentId));
		return INDEX_PAGE;
	}

	@GetMapping("/new")
	public String newExam(Model model) {
		model.addAttribute(EXAM_ATTRIBUTE_NAME, new Exam("", "", 0, 0, false));
		return ADD_EXAM_PAGE;
	}

	@PostMapping("/save")
	public String saveExam(Exam exam) {
		studentService.addExamToStudentById(getLoggedStudentId(), exam);
		return REDIRECT_TO_INDEX_PAGE;
	}

	@GetMapping("/delete")
	public String deleteExam(@RequestParam("id") String examName) {
		studentService.removeExamByNameToStudentById(getLoggedStudentId(), examName);
		return REDIRECT_TO_INDEX_PAGE;
	}

}
